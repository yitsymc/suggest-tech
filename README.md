# Suggestic Technical Exercise

Suggestic Technical Exercise

Steps:
1. Download project from repo

2. Run 
`docker-compose up`

3. Check if project is running in http://localhost:5000/

 `{
  "message": 
  "Suggestic Technical Ex."
}`

The project gives two endpoints:
  **POST '/api/convert_single_list/' with items**
  This endpoint return a list with results and save the request data
  
  Example:
  
`  curl --location --request POST 'http://localhost:5000/api/convert_single_list/' \
--header 'Content-Type: application/json' \
--data-raw '{
    "items": [1, 2,[3,4,[5,6],7],8]
}'`

Result:

`{
    "result": [1,2,3,4,5,6,7,8]
}`
  
  **GET '/api/get_previous_requests/'**
  This endpoint return all saved data
  
  Example:
  
  `GET curl --location --request GET 'http://localhost:5000/api/get_previous_requests/'`
  
  Result:
  
  `{
    "data": [
        {
            "items": [1, 2,[3,4,[5,6],7],8],
            "result": [1,2,3,4,5,6,7,8]
        },   
    ]
  }`