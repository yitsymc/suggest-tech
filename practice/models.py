from practice import db


class Requests(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    values = db.Column(db.JSON, nullable=True)

    def __repr__(self):
        return '<Result %r>' % ''
