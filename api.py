import json

from flask import jsonify
from flask_restful import Resource, Api
from flask import request
from practice import app
from practice.models import *

api = Api(app)


@app.route('/')
def hello():
    return jsonify({'message': 'Suggestic Technical Ex.'})


@app.route('/api/get_previous_requests/', methods=['GET'])
def get_previous_requests():
    """
    Method GET '/api/get_previous_requests/'
    :return: list of dictionaries with keys 'items' and 'result'
    """
    requests = Requests.query.order_by(Requests.id.desc()).all()
    data = [{'items': request.values['items'],
             'result': request.values['result']} for request in requests] if requests else {}
    return jsonify({'data': data})


@app.route('/api/convert_single_list/', methods=['POST'])
def convert_single_list():
    """
    Method POST '/api/convert_single_list/'
    :param items: list of values
    :return: result, one list of values if ok or empty
    """
    result = []
    data = json.loads(request.data.decode('utf-8'))
    if 'items' in data:
        str_dict = list(map(str, data['items']))
        result_list = []
        for item in str_dict:
            result_list += item.replace('[', '').replace(']', '').split(',')
        result = list(map(int, result_list))
        requests_model = Requests()
        requests_model.values = dict(items=data['items'], result=result)

        db.session.add(requests_model)
        db.session.commit()
    return jsonify({'result': result})


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
